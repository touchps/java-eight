package lambda;

import java.io.File;


/**
 * Interface variants use of lambda expression
 * @author pupusing
 *
 */
/**
 * the classic interface
 */
interface FileFilter {
   boolean accept(File file);
}

class JavaFilter implements FileFilter {
   public boolean accept(File file) {
      return file.getName().endsWith(".java");
   }
}


public class InterfaceLam {
   public static void main(String[] args) {
      /**
       * anonymous class using same filter interface (Most used way)
       * use to declare at same place where its going to be used
       * sideeffect is it can clutter
       */
      FileFilter fileFilter = new FileFilter() {

         @Override
         public boolean accept(File file) {
            // TODO Auto-generated method stub
            return file.getName().endsWith(".java");
         }
         
      };
      
      /**
       * Lambda expression: more readeable and writable way of anonymous class
       */
      FileFilter filterLambda = (File file) -> file.getName().endsWith(".java");
   }
}

